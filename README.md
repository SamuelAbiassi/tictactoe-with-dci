# A Data-Context-Interaction example applied to TicTacToe

This repository aims to demonstrate how a TicTacToe game could be implemented using the [Data-Context-Interaction (DCI) paradigm](https://en.wikipedia.org/wiki/Data,_context_and_interaction).

The implementation is widely based on [Andreas Söderlund](https://hashnode.com/@encodeart) awesome introductory articles on the [DCI paradigm with Typescript](https://blog.encodeart.dev/series/dci-typescript-tutorial).
