export const scoreStore: {
    state: { winner: string; loser: string }[]
    update: ({ winner, loser }: { winner: string; loser: string }) => void
} = {
    state: [],
    update: ({ winner, loser }: { winner: string; loser: string }) => {
        scoreStore.state.push({ winner, loser })
    },
}
