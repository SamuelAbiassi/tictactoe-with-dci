export function Scene<Actors, PublicAPI>(script: (actors: Actors) => PublicAPI): (actors: Actors) => PublicAPI
export function Scene<Actors, PublicAPI>(
    script: (actors: Actors) => PublicAPI,
    preconditions?: { assertion: (config: any) => boolean; errorMessage: string }[],
    postconditions?: { assertion: (config: any) => boolean; errorMessage: string }[]
): ((actors: Actors) => PublicAPI) | UnplayableScene {
    if (preconditions === undefined && postconditions === undefined) {
        return script
    }

    try {
        verifyPreconditions(preconditions!)

        return script
    } catch (e) {
        console.error(e)
        return () => undefined
    }
}

type UnplayableScene = () => undefined

function verifyPreconditions(preconditions: { assertion: (config: any) => boolean; errorMessage: string }[]): void {
    preconditions.forEach((condition: { assertion: (config: any) => boolean; errorMessage: string }) => {
        if (!condition.assertion) {
            throw new Error(condition.errorMessage)
        }
    })
}
