export { RoleCueScript as RoleCueScripts, assignRole, detachRoles } from './role'
export { Scene } from './scene'

export type { Role } from './role'