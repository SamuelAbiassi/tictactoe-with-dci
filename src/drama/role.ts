export function RoleCueScript(parts: RoleParts[]): RoleCueScript {
    const roleScript: RoleCueScript = {};

    parts.forEach((part) => {
        roleScript[part.cue] = part.script;
    });

    return roleScript;
}

export function assignRole<ActorType, RoleCueScript>(
    objectToBind: ActorType,
    roleMethods: RoleCueScript
): Role<ActorType, RoleCueScript> {
    if (objectToBind === undefined) {
        console.error("Role assignation failed: object to bind was undefined");
    }

    if (typeof objectToBind !== "object") {
        console.error("Role assignation failed: object to bind was not an object");
    }

    const roleAssignedActor = assignRoleMethodToActor<ActorType, RoleCueScript>(
        objectToBind,
        roleMethods
    );

    const detachableReadyRoleAssignedActor = storeRoleMethodSymbols(
        roleAssignedActor,
        roleMethods
    );

    const detachableRoleAssignedActor = bindDetachMethod(
        detachableReadyRoleAssignedActor
    );

    return detachableRoleAssignedActor;
}

export function detachRoles(roles: DetachableRole[]): void {
    roles.forEach(role => {
        role.detachRole()
    })
}

type RoleParts = {
    cue: symbol;
    script: (...args: any) => any;
}

type RoleCueScript = Record<symbol, (...args: any) => any>;

function bindDetachMethod<ActorType, RoleCueScript>(
    detachableReadyRoleAssignedActor: DetachableReadyRoleAssignedActor<
        ActorType,
        RoleCueScript
    >
) {
    return Object.assign(detachableReadyRoleAssignedActor, {
        detachRole: detachRole.bind(detachableReadyRoleAssignedActor),
    }) as Role<ActorType, RoleCueScript>;
}

function assignRoleMethodToActor<ActorType, RoleCueScript>(
    objectToBind: ActorType,
    roleMethods: RoleCueScript
): RoleAssignedActor<ActorType, RoleCueScript> {
    return Object.assign(objectToBind as Object, roleMethods) as Role<
        ActorType,
        RoleCueScript
    >;
}

function detachRole<ActorType, RoleCueScript>(
    this: DetachableReadyRoleAssignedActor<ActorType, RoleCueScript>
): void {
    this.roleMethodSymbols.forEach((symbol) => {
        delete (this as any)[symbol];
    });
}

function storeRoleMethodSymbols<ActorType, RoleCueScript>(
    role: RoleAssignedActor<ActorType, RoleCueScript>,
    roleMethods: RoleCueScript
): DetachableReadyRoleAssignedActor<ActorType, RoleCueScript> {
    const roleMethodSymbols = Object.getOwnPropertySymbols(roleMethods);

    const detachableReadyRoleAssignedActor = Object.assign(role as object, {
        roleMethodSymbols: roleMethodSymbols,
    }) as DetachableReadyRoleAssignedActor<ActorType, RoleCueScript>;

    return detachableReadyRoleAssignedActor;
}

type RoleAssignedActor<ActorType, RoleCueScript> = ActorType & RoleCueScript;

type DetachableReadyRoleAssignedActor<ActorType, RoleCueScript> =
    RoleAssignedActor<ActorType, RoleCueScript> & RoleMethodKeeper;

type RoleMethodKeeper = { roleMethodSymbols: symbol[] };

type DetachableRole = {
    detachRole: (
        this: DetachableRole
    ) => void
};

export type Role<ActorType, RoleCueScript> = ActorType &
    RoleCueScript &
    DetachableRole &
    RoleMethodKeeper;
