import { assignRole } from '../drama/role'

export function checkIfCurrentGameIsADraw(Board: { state: string[][] }, judge = {}): boolean {
    // === JUDGE ROLE ===

    const checkForADraw = Symbol('Check if there is nowhere left to play on a board')

    const judgeRole = {
        [checkForADraw]: (): boolean => {
            Judge.detachRole()

            return Board.state.every((row) => row.every((playerSign) => playerSign !== ''))
        },
    }

    const Judge = assignRole(judge, judgeRole)

    // CONTEXT LAUNCH
    {
        return Judge[checkForADraw]()
    }
}
