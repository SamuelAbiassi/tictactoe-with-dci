import { assignRole, detachRoles, RoleCueScripts, Scene } from '../../drama'
import { checkIfCurrentGameIsADraw } from '../check-tictactoe-board-for-draw'
import { checkIfCurrentPlayerWonTicTacToe } from '../check-tictactoe-board-for-winner'
import { logTicTacToeScores } from '../log-tictactoe-scores'

export type TicTacToeGamePublicAPI = {
    play: () => void
    registerPlayerMove: (row: number, column: number) => void
}

type Player = {
    sign: string
    name: string
}

export type TicTacToeGameActors = {
    board: {
        state: string[][]
    }
    PlayerOne: Player
    PlayerTwo: Player
    infoSpace: {
        displayMessage: (message: string) => void
    }
    restartButton: {
        display: () => void
        hide: () => void
    }
    game?: {
        currentPlayer: Player
        winner: Player | null
        ended: boolean
    }
    logger?: {
        log: (scoreAnnouce: string) => void
    }
}

export const TicTacToeGame = Scene(Context)

function Context({
    board,
    PlayerOne,
    PlayerTwo,
    infoSpace,
    restartButton,
    game = {
        currentPlayer: PlayerOne,
        winner: null,
        ended: false,
    },
    logger,
}: TicTacToeGameActors): TicTacToeGamePublicAPI {
    // === RESTART BUTTON ROLE ===

    const hide = Symbol('Hide the "Restart the game" button')
    const show = Symbol('Show the "Restart the game" button')

    // === LOG SYSTEM ROLE ===

    const log = Symbol('Log the game score')

    // ===================

    // ==== INFO SPACE ROLE ===

    const announceGameStart = Symbol()
    const askForNextPlayerToPlay = Symbol()
    const displayWinner = Symbol()
    const displayDraw = Symbol()
    const warnForWrongSlotSelection = Symbol()

    const InfoSpace = assignRole(
        infoSpace,
        RoleCueScripts([
            {
                cue: announceGameStart,
                script: () => {
                    InfoSpace.displayMessage('Start the game!')
                },
            },
            {
                cue: askForNextPlayerToPlay,
                script: () => {
                    InfoSpace.displayMessage(`${Game.currentPlayer.name}, please play`)
                },
            },
            {
                cue: displayWinner,
                script: () => {
                    InfoSpace.displayMessage(`The game is over! ${Game.currentPlayer.name} has won!`)
                    Game[stop]()
                },
            },
            {
                cue: displayDraw,
                script: () => {
                    InfoSpace.displayMessage(`The game is a draw.`)
                    Game[stop]()
                },
            },
            {
                cue: warnForWrongSlotSelection,
                script: () => {
                    InfoSpace.displayMessage(`This slot can't be picked. Please select another one.`)
                },
            },
        ])
    )

    // === GAME ROLE ===

    const checkIfGameIsOver = Symbol('Check if the game is over by winning or draw')
    const checkIfPlayerChoiceIsValid = Symbol('Check if the player has chosen an empty space')
    const switchToNextPlayer = Symbol('Switch from Player one to Player two and backward')
    const stop = Symbol('Stop the game execution by cutting the players move registration')

    const Game = assignRole(
        game,
        RoleCueScripts([
            {
                cue: checkIfGameIsOver,
                script: () => {
                    if (checkIfCurrentPlayerWonTicTacToe(Board)) {
                        Game.winner = Game.currentPlayer
                        InfoSpace[displayWinner]()
                    } else if (checkIfCurrentGameIsADraw(Board)) {
                        InfoSpace[displayDraw]()
                    } else {
                        Game[switchToNextPlayer]()
                    }
                },
            },
            {
                cue: checkIfPlayerChoiceIsValid,
                script: (row: number, column: number) => {
                    const isValid = Board.state[row][column] === ''

                    if (isValid) {
                        Board[placePlayerSign](row, column)
                    } else {
                        InfoSpace[warnForWrongSlotSelection]()
                    }
                },
            },
            {
                cue: switchToNextPlayer,
                script: () => {
                    Game.currentPlayer === PlayerOne
                        ? (Game.currentPlayer = PlayerTwo)
                        : (Game.currentPlayer = PlayerOne)
                    InfoSpace[askForNextPlayerToPlay]()
                },
            },
            {
                cue: stop,
                script: () => {
                    Game.ended = true
                    Logger[log]()
                },
            },
        ])
    )

    // === BOARD ROLE ===

    const placePlayerSign = Symbol('Update the state of the board so that it can be rerendered')

    const boardRole = {
        [placePlayerSign]: (row: number, column: number) => {
            Board.state[row][column] = Game.currentPlayer.sign
            Game[checkIfGameIsOver]()
        },
    }
    const Board = assignRole(board, boardRole)

    const restartButtonRole = {
        [hide]: () => {
            RestartButton.hide()
            InfoSpace[announceGameStart]()
        },

        [show]: () => {
            RestartButton.display()
            detachRoles([Board, Game, InfoSpace, RestartButton, Logger])
        },
    }
    const RestartButton = assignRole(restartButton, restartButtonRole)

    const loggerRole = {
        [log]: () => {
            logTicTacToeScores({ PlayerOne, PlayerTwo, logger, Game })

            RestartButton[show]()
        },
    }
    const Logger = assignRole(logger ?? {}, loggerRole)

    // CONTEXT PUBLIC API
    {
        return {
            play: () => InfoSpace[announceGameStart](),
            registerPlayerMove: (row: number, column: number) => {
                if (!Game.ended) {
                    Game[checkIfPlayerChoiceIsValid](row, column)
                }
            },
        }
    }
}
