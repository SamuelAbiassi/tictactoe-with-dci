import { assignRole } from '../drama/role'

export const logTicTacToeScores = ({
    PlayerOne,
    PlayerTwo,
    Game,
    logger,
}: {
    PlayerOne: {
        sign: string
        name: string
    }
    PlayerTwo: {
        sign: string
        name: string
    }
    Game: {
        winner: typeof PlayerOne | typeof PlayerTwo | null
    }
    logger?: {
        log: (scoreAnnouce: string) => void
    }
}): void => {
    if (!logger) {
        return
    }

    // LOGGER ROLE

    const logScore = Symbol('Log the score of the game')

    // ==================

    const loggerRole = {
        [logScore]: () => {
            if (Game.winner) {
                Logger.log(`${Game.winner.name} won!`)
            } else {
                Logger.log(`The game is a draw`)
            }
        },
    }
    const Logger = assignRole(logger, loggerRole)

    const DetachRoles = () => {
        Logger.detachRole()
    }

    {
        Logger[logScore]()
        DetachRoles()
    }
}
