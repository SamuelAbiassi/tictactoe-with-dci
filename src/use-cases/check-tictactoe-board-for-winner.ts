import { assignRole } from '../drama/role'

export function checkIfCurrentPlayerWonTicTacToe(Board: { state: string[][] }, judge = {}): boolean {
    // === JUDGE ROLE ===

    const checkIfCurrentPlayerWon = Symbol('Check if current player has won the game')

    const judgeRole = {
        [checkIfCurrentPlayerWon]: () => {
            Judge.detachRole()

            return checkForAWinner()
        },
    }

    const Judge = assignRole(judge, judgeRole)

    const checkForAWinner = (): boolean => {
        return checkForWinnerOnEveryLine() || checkForWinnerOnEveryColumn() || checkDiagonalyForWinner()
    }

    const checkForWinnerOnEveryLine = (): boolean => {
        return checkForWinnerOnLine(0) || checkForWinnerOnLine(1) || checkForWinnerOnLine(2)
    }

    const checkForWinnerOnEveryColumn = (): boolean => {
        return checkForWinnerOnColumn(0) || checkForWinnerOnColumn(1) || checkForWinnerOnColumn(2)
    }

    const checkForWinnerOnLine = (lineIndex: number): boolean => {
        const boardLine = Board.state[lineIndex]
        return boardLine.every((playerSign) => boardLine[0] === playerSign && playerSign !== '')
    }

    const checkForWinnerOnColumn = (columnIndex: number): boolean => {
        const boardColumn = [Board.state[0][columnIndex], Board.state[1][columnIndex], Board.state[2][columnIndex]]

        return boardColumn.every((playerSign) => boardColumn[0] === playerSign && playerSign !== '')
    }

    const checkDiagonalyForWinner = (): boolean => {
        const boardFirstDiagonal = [Board.state[0][0], Board.state[1][1], Board.state[2][2]]
        const boardSecondDiagonal = [Board.state[2][0], Board.state[1][1], Board.state[0][2]]
        return (
            boardFirstDiagonal.every((playerSign) => boardFirstDiagonal[0] === playerSign && playerSign !== '') ||
            boardSecondDiagonal.every((playerSign) => boardSecondDiagonal[0] === playerSign && playerSign !== '')
        )
    }

    // CONTEXT LAUNCH
    {
        return Judge[checkIfCurrentPlayerWon]()
    }
}
